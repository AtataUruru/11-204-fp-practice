
-- | Нахождение максимум и его индекс 
-- >>> myMaxIndex [1,5,2,3,4]
-- (1,5)
myMaxIndex :: [Integer] -> (Int, Integer)
myMaxIndex [] = (-1, 0)
myMaxIndex (x:xs) = maxIndex (0, x) 1 xs 
	where
		maxIndex obj k [] = obj
	 	maxIndex obj k (x:xs) | x > (snd obj) =  maxIndex (k, x) (k + 1) xs
	 						  | otherwise  = maxIndex obj (k + 1) xs

-- | Количество элементов, равных максимальному
-- >>> maxCount [1,5,3,10,3,10,5]
-- 2
maxCount :: [Integer] -> Int
maxCount (x:xs) = maxIndex (1, x) xs 
	where
		maxIndex obj [] = fst obj
	 	maxIndex obj (x:xs) | x > (snd obj) =  maxIndex (1, x) xs
	 						| x == (snd obj) =  maxIndex (fst obj + 1, x) xs
	 						| otherwise  = maxIndex obj xs

-- | Количество элементов между минимальным и максимальным
-- >>> countBetween [-1,3,100,3]
-- 2
--
-- >>> countBetween [100,3,-1,3]
-- -2
--
-- >>> countBetween [-1,100]
-- 1
--
-- >>> countBetween [1]
-- 0

myMinIndex :: [Integer] -> (Int, Integer)
myMinIndex [] = (-1, 0)
myMinIndex (x:xs) = minIndex (0, x) 1 xs 
	where
		minIndex obj k [] = obj
	 	minIndex obj k (x:xs) | x < (snd obj) =  minIndex (k, x) (k + 1) xs
	 						  | otherwise  = minIndex obj (k + 1) xs

countBetween :: [Integer] -> Int
countBetween [] = 0
countBetween [x] = 0
countBetween mass = (fst $ myMaxIndex mass ) - (fst $ myMinIndex mass)

main = print $ "Test your luck"