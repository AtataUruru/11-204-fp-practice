-- | deletes first arg from second arg
-- >>> deleteInt 1 [1,2,1,3,4]
-- [2,3,4]
--
-- >>> deleteInt 1 [2,3]
-- [2,3]
--
-- >>> deleteInt 1 []
-- []
deleteInt :: Int -> [Int] -> [Int]
deleteInt n array = filter (/=n) array

-- | returns list of indices of first arg in second arg
-- >>> findIndices 1 [1,2,1,3,4]
-- [0,2]
--
-- >>> findIndices 1 [2,3]
-- []
findIndicesInt n [] = []
findIndicesInt n (x:xs) | x == n = length xs - 1 : findIndicesInt n xs
						| otherwise = findIndicesInt n xs

-- | tribo_n = tribo_{n-1} + tribo_{n-2} + tribo_{n-3}
--   tribo_0 = 1
--   tribo_1 = 1
--   tribo_2 = 1
--
-- >>> tribo 0
-- 1
--
-- >>> tribo 1
-- 1
--
-- >>> tribo 2
-- 1
--
-- >>> tribo 3
-- 3
--
-- >>> odd (tribo 100)
-- True
tribo n = _tribo 1 1 1
  where _tribo 0 a b c = a
        _tribo 1 a b c = b
        _tribo 2 a b c = c
        _tribo n a b c = _tribo (n-1) b c (a+b+c)

-- Тип Цвет, который может быть Белым, Черным, Красным, Зелёным, Синим, либо Смесь из трёх чисел (0-255)
-- операции сложение :: Цвет -> Цвет -> Цвет
-- (просто складываются интенсивности, если получилось >255, то ставится 255)
-- ПолучитьКрасныйКанал, ПолучитьЗелёныйКанал, ПолучитьСинийКанал :: Цвет -> Int
data Color =  White
              | Black
              | Red
              | Green
              | Blue
              | RGB { red
                        , green
                        , blue :: Integer
                        }
              deriving Show

getColor :: Color -> Color
getColor _color =
  case _color of
        White -> RGB { red = 255, green = 255, blue = 255 }
        Black -> RGB { red = 0, green = 0, blue = 0 }
        Red -> RGB { red = 255, green = 0, blue = 0 }
        Green -> RGB { red = 0, green = 255, blue = 0 }
        Blue -> RGB { red = 0, green = 0, blue = 255 }
        otherwise -> _color

sum :: Color -> Color -> Color
sum c1 c2 = RGB { red = if r < 255 then r else 255
                            , green = if g < 255 then g else 255
                            , blue = if b < 255 then b else 255
                          }
                  where
                    rgbC1 = getColor(c1)
                    rgbC2 = getColor(c2)
                    r = red rgbC1 + red rgbC2
                    g = green rgbC1 + green rgbC2
                    b = blue rgbC1 + blue rgbC2

getRed :: Color -> Integer
getRed c = red (getColor c)

getGreen :: Color -> Integer
getGreen c = green (getColor c)

getBlue :: Color -> Integer
getBlue c = blue (getColor c)

main = do
    print (show(deleteInt 1 [3,4,5,6,7,2,1]))
    print (show(findIndicesInt 1 [1,2,3,4,5,2,1,2]))
