-- арифметические прогрессии длины 3 с расстоянием k
-- например, head (arith 2) = [3,7,11]
arith :: Int -> [[Int]]
nat = 1 : map (+1) nat
isPrime 1 = False
isPrime 2 = True
isPrime n | even n = False
          | otherwise = not $ any (\i -> n`mod`i == 0) $ takeWhile (\i -> i * i <= n) [3,5..]
primes = filter isPrime nat

isArith :: (Ord a, Num a) => (a, (a, a)) -> Bool
isArith t = num2 - num1 == num3 - num2 where
	num1 = fst t
	num2 = fst (snd t)
	num3 = snd (snd t)

iterWithFilter :: (Ord b, Num b) => [(b, (b, b))] -> [[b]]
iterWithFilter [] = []
iterWithFilter (x:xs) | isArith x = [fst x, fst $ snd x, snd $ snd x]:iterWithFilter xs
							| otherwise = iterWithFilter xs
tailN 0 list = list
tailN n list = tailN (n - 1) (tail list)

arith k = iterWithFilter arr where
	sndList = tailN k primes
	arr = zip primes $ zip sndList (tailN k sndList)

-- положение ферзей на шахматной доске
-- список номеров горизонталей, на которых находятся ферзи
-- например, Board [1,2,3,4,5,6,8,7] -- это такое расположение
--  +--------+
-- 8|      ♕ |
-- 7|       ♕|
-- 6|     ♕  |
-- 5|    ♕   |
-- 4|   ♕    |
-- 3|  ♕     |
-- 2| ♕      |
-- 1|♕       |
-- -+--------+
--  |abcdefgh|

newtype Board = Board { unBoard :: [Int] } deriving (Eq,Show)

queens :: Int -> [Board]
queens n = filter test (generate n)
	where   generate :: Int -> [Board]
			generate 0 = [Board []]
			generate k = [ Board (q:unBoard qs) | q <- [1..n], qs <- generate (k-1)]
			test (Board []) = True
			test (Board (x:xs)) = isSafe x xs && test (Board xs)
			isSafe q qs = not (q `elem` qs || sameDiag q qs)
			sameDiag q qs = any (\(colDist, p) -> abs(q - p) == colDist) $ zip [1..] qs

-- Белые начинают и дают мат в два хода
-- 
-- Белые: Фf8 g4 Крh2
-- Черные: g5 h5 a4 Крh4
-- 
-- (написать перебор всех вариантов полуходов длины три,
-- вернуть список последовательностей полуходов, ведущих
-- к решению; до этого определить необходимые типы)
-- См. https://en.wikipedia.org/wiki/Chess_notation

--solutions :: [[Move]]