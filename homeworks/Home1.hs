{- 
 -
 -                4
 - pi = -------------------
 -                1^2
 -       1 + --------------
 -                   3^2
 -            2 + ---------
 -                     5^2
 -                 2 + ----
 -                      ...
 -}
pi1 :: Int -> Float
pi1 n | n > 0 = 4 / (1 + (_pi1 n 1))
    where _pi1 1 k = k * k
          _pi1 n k = (k * k) / (2 + (_pi1 (n - 1) (k + 2)))

{-
 -                  1^2
 - pi = 3 + -------------------
 -                   3^2
 -           6 + -------------
 -                     5^2
 -                6 + ------
 -                     ...
 -}
pi2 :: Int -> Float
pi2 n | n > 0 =  3 + (1/ (_pi2 n 3))
	where 
		_pi2 1 k = 6
		_pi2 n k = (6 + k * k) / ((_pi2 (n - 1) (k + 2)))


{-
 -                 4
 - pi = ------------------------
 -                   1^2
 -       1 + ------------------
 -                    2^2
 -            3 + -----------
 -                     3^2
 -                 5 + ---
 -                     ...
 -}
pi3 :: Int -> Float
pi3 n | n > 0 = 4 / (1 + (_pi3 n 1))
	where
		_pi3 1 k = k * k
		_pi3 n k = (k * k) / (1 + 2* k + (_pi3 (n - 1) (k + 2)))
		
{-       4     4     4     4
 - pi = --- - --- + --- - --- + ...
 -       1     3     5     7
 -}
pi4 :: Int -> Float
pi4 n | n > 0 = _pi4 n 1
	where 
		_pi4 1 k = 0
		_pi4 n k = 4 / k - 4 / (k + 2) + _pi4 (n - 1) (k + 4)

{-             4         4         4         4
 - pi = 3 + ------- - ------- + ------- - -------- + ...
 -           2*3*4     4*5*6     6*7*8     8*9*10
 -}
pi5 :: Int -> Float
pi5 n | n > 0 = _pi5 n 2
	where 
		_pi5 1 k = 3
		_pi5 n k = 4/ ( k * (k + 1) * (k + 2) ) - 4 / ( (k+4) * (k + 3) * (k + 2) ) + _pi5 (n - 1) (k + 4 )
{-
       x^1     x^2
e^x = ----- + ----- + ...
        1!      2!
-}
--fac :: Int -> Int
fac n = foldr (*) 1 [1..n]

--e :: Int -> Int -> Float
e x n | n > 0  =  _e x n 1
	where
--        _e :: Int -> Int -> Int -> Float
		_e x 1 k = x
		_e x n k  = x**k / fac (k) + _e (x) (n-1) (k+1)

main = do
    print $ pi1 5
    print $ pi2 5
    print $ pi3 5
    print $ pi4 5
    print $ pi5 5
    print $ e 2 20