data Expr = Val Int
          | Plus Expr Expr
          | Minus Expr Expr
          | Mult Expr Expr
          | Div Expr Expr
          | Mod Expr Expr

eval :: Expr -> Int
eval (Val x) = x
eval (Plus x y) = eval x + eval y
eval (Minus x y) = eval x - eval y
eval (Mult x y) = eval x * eval y
eval (Div x y) = (eval x) `div` (eval y)
eval (Mod x y) = (eval x) `mod` (eval y)

main = do
    print $ "Test your luck"
    print $ Plus Val 3 Val 5