import Test.Hspec

data Expression = Literal String
                | Lambda String Expression
                | Apply Expression Expression
                deriving (Eq, Show)


fn_for_Expression :: Expression -> a
fn_for_Expression exp = case exp of
                    Literal str -> undefined str
                    Lambda str1 exp3 -> undefined str1 (fn_for_Expression exp3)
                    Apply exp1 exp2 -> undefined (fn_for_Expression exp1) (fn_for_Expression exp2)


type Context = [(String, Expression)]


fn_for_Context :: Context -> a
fn_for_Context cont = undefined (head cont) fn_for_Context (tail cont)

eval :: Expression -> Expression
eval t = eval'[] t

eval' :: Context -> Expression -> Expression
eval' context exp = case exp of
                l@(Literal a)   -> maybe l id (lookup a context)
                (Lambda s t)    -> Lambda s (eval' context t)
                (Apply t1 t2)   -> apply context (eval' context t1) (eval' context t2)

apply :: Context -> Expression -> Expression -> Expression
apply context t1 t2 = case t1 of
                    (Lambda s t)    ->  eval' ((s, t2):context) t
                    otherwise       ->  Apply t1 t2





main = hspec $ do
  describe "Simple tests should pass" $ do
    it "(λx . λy . x) (λa . a) ((λx . x x) (λx . x x)) --> λa.a" $
       (eval $ (Apply (Apply (Lambda "x" (Lambda "y" (Literal "x"))) (Lambda "a" (Literal "a"))) (Apply (Lambda "x" (Apply (Literal "x") (Literal "x"))) (Lambda "x" (Apply (Literal "x") (Literal "x")))))) `shouldBe` Lambda "a" (Literal "a")
    it "((λx.x) x)  ->  x" $
       (eval $ Apply (Lambda "x" (Literal "x")) (Literal "x")) `shouldBe` Literal "x"
    it "((λx.(λy.y)) (λz.z))  ->  (λy.y)" $
       (eval $ Apply (Lambda "x" (Lambda "y" (Literal "y"))) (Lambda "z" (Literal "z"))) `shouldBe` Lambda "y" (Literal "y")
  describe "Complex tests should not fail" $ do
    it "(\\x . \\y . x (\\y . y)) (\\x . \\y . x)" $
       (eval $ Apply (Lambda "x" $ Lambda "y" $ Apply (Literal "x") (Lambda "y" $ Literal "y")) (Lambda "x" $ Lambda "y" $ Literal "x")) `shouldBe` Lambda "y" (Lambda "x" $ Lambda "y'" $ Literal "x")

